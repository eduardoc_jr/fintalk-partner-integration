const logger = require("payment-pkg/base/Logger");
const flow  = require("payment-pkg/base/FlowControl");
const request = require("sync-request");

class Controller {

    sampleRequest(event) {
        try {
            if (event == null || event.body.url == undefined) throw new Error("Malformed request"); 
            const response =  request("GET", `http://${event.body.url}`);
      
            if(response != null && response.statusCode == 200) {
                return flow.success(JSON.parse(response.getBody()));
            } else {
              logger.log("Request Error => " + String(response.statusCode) + JSON.stringify(body));
              return flow.customStatusCode(response.statusCode, {});
            }
        } catch(error) {
            return flow.error(error);      
        }
    }

    routeFunction(customer, endpoint, ...variables) {
        var func = "this." + endpoint;
        let event = {};

        if (Object.getOwnPropertyNames(Object.getPrototypeOf(this)).includes(endpoint)) {
            var params = {};
            if (variables[0] != null) {
                variables[0].forEach(function(variable) {
                    var t = variable.split(":");
                    params[t[0]] = t[1].trim();
                });
            }
            
            if (Object.keys(params).length !== 0) {
                event.body = params;
            }

            event.customer = customer;
            var ret = eval(func + "(event);");
            if (ret.body && typeof ret.body === "string") {
                try {
                    ret.body = JSON.parse(ret.body);
                } catch(ex) {
                    logger.log("Feng error => " + ex.message + " - " + JSON.stringify(ret));
                }
            }
            return ret;
        } else {
            throw new Error("Unhandled route");
        }
    }
}

module.exports = Controller;