const logger = require("payment-pkg/base/Logger");
const customerId = "b09e7b60-1169-11ea-9a9f-362b9e155667";

const cmd = `bot("sampleRequest", "url: www.mocky.io/v2/5ddf006c310000cb723ae5e9")`;

const bot = (endpoint, ...variables) => {
    const client = require(`./controller/controller.js`);
    const controller = new client();
    try {
        let request = controller.routeFunction(customerId, endpoint, variables);
        if (request.body && typeof request.body.result === "boolean") {
            return request.body.result;
        } else {
            let ret = {};
            if (request.body && request.body.result) {
                ret = JSON.parse(JSON.stringify(request.body.result));
            } else {
                ret = request.body;
            }
            return ret;
        }
    } catch(err) {
        logger.log("Bot error => " + err.message);
        return {text: err.message, type: "text", priority: 0};
    }
}

module.exports = { bot }

logger.log(eval(cmd));